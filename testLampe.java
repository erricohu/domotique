import static org.junit.Assert.*;

import org.junit.Test;


public class testLampe {

	@Test
	public void testAjout() {
		Lampe l1 = new Lampe("lampe");
		Telecommande t = new Telecommande();
		t.ajouterMulti(l1);
		assertEquals("mauvais ajout", l1, t.getMulti(0));
	}
	
	@Test
	public void testAjoutv1() {
		Lampe l1 = null;
		Telecommande t = new Telecommande();
		t.ajouterMulti(l1);
		assertEquals("mauvais ajout", l1, t.getMulti(0));
	}
	
	@Test
	public void testActivation() {
		Lampe l1 = new Lampe("lampe");
		Telecommande t = new Telecommande();
		t.ajouterMulti(l1);
		t.activerMulti(l1);
		assertEquals("mauvaise activation", true , l1.isAllume());
	}
	
	@Test
	public void testActivationv1() {
		Lampe l1 = new Lampe("lampe");
		Telecommande t = new Telecommande();
		t.ajouterMulti(l1);
		t.activerMulti(l1);
		t.activerMulti(l1);
		assertEquals("mauvaise activation", true , l1.isAllume());
	}
	
	@Test
	public void testDesactivation() {
		Lampe l1 = new Lampe("lampe");
		Telecommande t = new Telecommande();
		t.ajouterMulti(l1);
		t.desactiverMulti(l1);
		assertEquals("mauvaise desactivation", false , l1.isAllume());
	}
	
	@Test
	public void testDesactivationv1() {
		Lampe l1 = new Lampe("lampe");
		Telecommande t = new Telecommande();
		t.ajouterMulti(l1);
		t.desactiverMulti(l1);
		t.desactiverMulti(l1);
		assertEquals("mauvaise desactivation", false , l1.isAllume());
	}
	
	@Test
	public void testActiveTout() {
		Lampe l1 = new Lampe("lampe");
		Lampe l2 = new Lampe("lampe2");
		Lampe l3 = new Lampe("lampe3");
		Telecommande t = new Telecommande();
		t.ajouterMulti(l1);
		t.ajouterMulti(l2);
		t.ajouterMulti(l3);
		t.activerTout();
		assertEquals("mauvaise activation", true, l1.isAllume());
		assertEquals("mauvaise activation", true, l2.isAllume());
		assertEquals("mauvaise activation", true, l3.isAllume());
	}
	
	@Test
	public void testString() {
		Lampe l1 = new Lampe("toto");
		Lampe l2 = new Lampe("titi");
		Telecommande t = new Telecommande();
		t.ajouterMulti(l1);
		t.ajouterMulti(l2);
		String res2 = "Nom de la lampe : toto Statut : false\nNom de la lampe : titi Statut : false\n";
		String res = "Nom de la lampe : toto Statut : false";
		assertEquals("caca", res, l1.toString());
		assertEquals("caca", res2, t.toString());
	}

}
