
public class AdapteurLumiere implements Multimedia{

	
	private Lumiere lumiere;
	
	public AdapteurLumiere(){
		this.lumiere = new Lumiere();
	}
	public void allumer(){
		this.lumiere.changerIntensite(100);
	}
	
	public void eteindre(){
		this.lumiere.changerIntensite(0);
	}
	
	public String toString(){
		return(this.lumiere.toString());
	}
	
	public boolean isAllume(){
		if (this.lumiere.getLumiere() == 0)
			return(false);
		else
			return(true);
	}
	public String getNom(){
		return("Lumiere");
	}
}
