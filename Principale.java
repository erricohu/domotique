import java.util.*;
public class Principale {

	public static void main(String []args){
		Telecommande t = new Telecommande();
		Lampe l1 = new Lampe("Lampe 1");
		Lampe l2 = new Lampe("Lampe 2");
		Lampe l3 = new Lampe("Lampe 3");
		Lampe l4 = new Lampe("Lampe 4");
		Hifi h1 =  new Hifi("Hifi 1");
		Hifi h2 =  new Hifi("Hifi 2");
		Hifi h3 =  new Hifi("Hifi 3");
		Hifi h4 =  new Hifi("Hifi 4");
		t.ajouterMulti(l1);
		t.ajouterMulti(l2);
		t.ajouterMulti(l3);
		t.ajouterMulti(l4);
		t.ajouterMulti(h1);
		t.ajouterMulti(h2);
		t.ajouterMulti(h3);
		t.ajouterMulti(h4);
		Scanner sc = new Scanner(System.in);
		boolean exit = true;
		while (exit){
		for (int i = 0; i < 8; i++){
			if ((t.getMulti(i)).isAllume())	
				System.out.println(i + "-" + (t.getMulti(i)).getNom() + ": " + "On");
			else
				System.out.println(i + "-" + (t.getMulti(i)).getNom() + ": " + "Off");
		}
		System.out.println("Entrer commande (+/-/Exit)");
		String com = sc.nextLine();
		int nb;
		if (com.equals("Exit"))
			exit = false;
		else if (com.equals("+")){
			System.out.println("Entrer le numero de la lampe ou -1 pour tout");
			com = sc.nextLine();
			if (com.equals("-1"))
				t.activerTout();
			else{
				nb = Integer.parseInt(com);
				Multimedia m = t.getMulti(nb);
				t.activerMulti(m);
			}
		}
		else if (com.equals("-")){
			System.out.println("Entrer le numero de la lampe");
			com = sc.nextLine();
			nb = Integer.parseInt(com);
			Multimedia m = t.getMulti(nb);
			t.desactiverMulti(m);
			}
		else
			System.out.println("Mauvaise commande");
		}
		sc.close();
	}
}