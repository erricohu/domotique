/**
* classe qui permet de modeliser un variateur de lumiere
* ce classe doit etre utilisee avec la telecommande
*
* ATTENTION: CONTRAINTE IMPORTANTE
* CETTE CLASSE NE DOIT JAMAIS ETRE MODIFIEE
*(IMAGINEZ QUE VOUS NE DISPOSEZ DU .CLASS)
*/
public class Lumiere {
/**
* intensite de la lumiere modulable par le variateur
* valeur comprise entre 0 et 100;
*/
	int intensite ;
/**
* constructeur par defaut
* un variateur eteint
*/
	public Lumiere ()
	{
		this. intensite =0;
	}
/**
* permet de changer l'intensite du variateur
* @param i nouvelle intensite du variateur
*/
	public void changerIntensite (int i )
	{
		if (i >=0&& i <=100)
			this. intensite = i ;
	}
/**
* retourne l'intensite du variateur
* @return intensite de la lumiere
*/
	public int getLumiere ()
	{
		return this. intensite ;
	}
/**
* methode toString , affiche lumiere et la valeur intensite
* @return description de l'objet this
*/
	public String toString ()
	{
		return("lumiere: "+ intensite );
	}
}
