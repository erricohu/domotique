public class Lampe  implements Multimedia{

	private String nom;
	private boolean allume;
	
	public Lampe(String n){
		this.nom = n;
		this.allume = false;
	}
	
	public void allumer(){
		this.allume = true;
	}
	
	public void eteindre(){
		this.allume = false;
	}
	
	public String toString(){
		return("Nom de la lampe : " + this.nom + " Statut : " + this.allume);
	}
	
	public boolean isAllume(){
		return(this.allume);
	}
	
	public String getNom(){
		return (this.nom);
	}
}

