public class Hifi implements Multimedia{
	/**
	* classe qui represente une chaine hifi	
	**/
	/**
	* l'intensite du son de la chaine
	** on suppose que la chaine est eteinte si le son vaut 0
	*/
	private int son = 0;
	private String nom;
	
	public Hifi(String n){
		this.nom = n;
	}
	/**
	* permet d'allumer ou d'augmenter le son de la chaine
	*/
	public void allumer () {
		this. son += 10;
	//son maximum
		if (this. son > 100)
			this. son = 100;
	}
		
	/**
	* permet d'eteindre la chaine (son a 0)
	*/
	public void eteindre () {
		this. son = 0;
	}
	
	public String getNom(){
		return (this.nom);
	}
	
	public boolean isAllume(){
		return (this.son > 0);
	}
	/**
	* surcharge de la methode affichage
	* sous la forme "Hifi:10"
	*/
	public String toString ()
	{
		String r = "";
		r += "Hifi:" + son ;
		return( r );
	}
}
