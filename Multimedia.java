public interface Multimedia {
		
		public void allumer();
		public void eteindre();
		public String toString();
		public boolean isAllume();
		public String getNom();
}
