import static org.junit.Assert.*;

import org.junit.Test;


import static org.junit.Assert.*;

import org.junit.Test;


public class testLampeHifi {

	@Test
	public void testAjout() {
		Lampe l1 = new Lampe("lampe");
		Hifi h1 = new Hifi("hifi");
		Telecommande t = new Telecommande();
		t.ajouterMulti(l1);
		t.ajouterMulti(h1);
		assertEquals("mauvais ajout", l1, t.getMulti(0));
		assertEquals("mauvais ajout", h1, t.getMulti(1));
	}
	
	@Test
	public void testAjoutv1() {
		Lampe l1 = null;
		Hifi h1 = null;
		Telecommande t = new Telecommande();
		t.ajouterMulti(l1);
		t.ajouterMulti(h1);
		assertEquals("mauvais ajout", l1, t.getMulti(0));
		assertEquals("mauvais ajout", h1, t.getMulti(0));
	}
	
	@Test
	public void testActivation() {
		Lampe l1 = new Lampe("lampe");
		Hifi h1 = new Hifi("Hifi");
		Telecommande t = new Telecommande();
		t.ajouterMulti(l1);
		t.ajouterMulti(h1);
		t.activerMulti(l1);
		t.activerMulti(h1);
		assertEquals("mauvaise activation", true , l1.isAllume());
		assertEquals("mauvaise activation", true , h1.isAllume());
	}
	
	@Test
	public void testActivationv1() {
		Lampe l1 = new Lampe("lampe");
		Hifi h1 = new Hifi("Hifi");
		Telecommande t = new Telecommande();
		t.ajouterMulti(l1);
		t.activerMulti(l1);
		t.activerMulti(l1);
		t.ajouterMulti(h1);
		t.activerMulti(h1);
		t.activerMulti(h1);
		assertEquals("mauvaise activation", true , l1.isAllume());
		assertEquals("mauvaise activation", true , h1.isAllume());
	}
	
	@Test
	public void testDesactivation() {
		Lampe l1 = new Lampe("lampe");
		Hifi h1 = new Hifi("hifi");
		Telecommande t = new Telecommande();
		t.ajouterMulti(l1);
		t.desactiverMulti(l1);
		t.ajouterMulti(h1);
		t.desactiverMulti(h1);
		assertEquals("mauvaise desactivation", false , l1.isAllume());
		assertEquals("mauvaise desactivation", false , h1.isAllume());
	}
	
	@Test
	public void testDesactivationv1() {
		Lampe l1 = new Lampe("lampe");
		Hifi h1 = new Hifi("hifi");
		Telecommande t = new Telecommande();
		t.ajouterMulti(l1);
		t.desactiverMulti(l1);
		t.desactiverMulti(l1);
		t.ajouterMulti(h1);
		t.desactiverMulti(h1);
		t.desactiverMulti(h1);
		assertEquals("mauvaise desactivation", false , l1.isAllume());
		assertEquals("mauvaise desactivation", false , h1.isAllume());
	}
	
	@Test
	public void testActiveTout() {
		Lampe l1 = new Lampe("lampe");
		Lampe l2 = new Lampe("lampe2");
		Lampe l3 = new Lampe("lampe3");
		Hifi h1 = new Hifi("hifi");
		Hifi h2 = new Hifi("hifi2");
		Hifi h3 = new Hifi("hifi3");
		Telecommande t = new Telecommande();
		t.ajouterMulti(l1);
		t.ajouterMulti(l2);
		t.ajouterMulti(l3);
		t.ajouterMulti(h1);
		t.ajouterMulti(h2);
		t.ajouterMulti(h3);
		t.activerTout();
		assertEquals("mauvaise activation", true, l1.isAllume());
		assertEquals("mauvaise activation", true, l2.isAllume());
		assertEquals("mauvaise activation", true, l3.isAllume());
		assertEquals("mauvaise activation", true, h1.isAllume());
		assertEquals("mauvaise activation", true, h2.isAllume());
		assertEquals("mauvaise activation", true, h3.isAllume());
	}
	
	@Test
	public void testString() {
		Lampe l1 = new Lampe("toto");
		Lampe l2 = new Lampe("titi");
		Hifi h1 = new Hifi("hifi");
		Hifi h2 = new Hifi("hifi2");
		Telecommande t = new Telecommande();
		t.ajouterMulti(l1);
		t.ajouterMulti(l2);
		t.ajouterMulti(h1);
		t.ajouterMulti(h2);
		String res2 = "Nom de la lampe : toto Statut : false\nNom de la lampe : titi Statut : false\nHifi:0\nHifi:0\n";
		String res = "Nom de la lampe : toto Statut : false";
		String resh = "Hifi:0";
		assertEquals("mauvais1", res, l1.toString());
		assertEquals("mauvais2", res2, t.toString());
		assertEquals("mauvais3", resh, h1.toString());
	}

}