import java.util.*;


public class Telecommande {

	private List<Multimedia> multi;
	
	public Telecommande(){
		this.multi = new ArrayList<Multimedia>();
	}
	
	public void ajouterMulti(Multimedia l){
		multi.add(l);
	}
	
	public void activerMulti(Multimedia l){
		for (int i = 0; i < multi.size(); i++){
			if (multi.get(i).equals(l)){
				l.allumer();
			}
		}
	}
	
	public void desactiverMulti(Multimedia l){
		for (int i = 0; i < multi.size(); i++){
			if (multi.get(i).equals(l))
				l.eteindre();
			}
	}
		
	public void activerTout(){
		Multimedia l;
		for (int i = 0; i < multi.size(); i++){
			l = multi.get(i);
			l.allumer();
			multi.set(i, l);
		}
	}
	
	public String toString(){
		String res = "";
		Multimedia l;
		for (int i = 0 ; i < multi.size() ; i++){
			l = multi.get(i);
			res = res + l.toString() + "\n";
		}
		return (res);
	}
	
	public Multimedia getMulti(int indice){
		return(multi.get(indice));
	}
}