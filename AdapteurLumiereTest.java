import static org.junit.Assert.*;

import org.junit.Test;


public class AdapteurLumiereTest {

	@Test
	public void test() {
		AdapteurLumiere a1 = new AdapteurLumiere();
		Telecommande t = new Telecommande();
		t.ajouterMulti(a1);
		assertEquals("mauvais ajout", a1, t.getMulti(0));
	}
	@Test
	public void testAjoutv1() {
		AdapteurLumiere a1 = new AdapteurLumiere();
		Telecommande t = new Telecommande();
		t.ajouterMulti(a1);
		assertEquals("mauvais ajout", a1, t.getMulti(0));
	}
	
	@Test
	public void testActivation() {
		AdapteurLumiere a1 = new AdapteurLumiere();
		Telecommande t = new Telecommande();
		t.ajouterMulti(a1);
		t.activerMulti(a1);
		assertEquals("mauvaise activation", true , a1.isAllume());
	}
	
	@Test
	public void testActivationv1() {
		AdapteurLumiere a1 = new AdapteurLumiere();
		Telecommande t = new Telecommande();
		t.ajouterMulti(a1);
		t.activerMulti(a1);
		t.activerMulti(a1);
		assertEquals("mauvaise activation", true , a1.isAllume());
	}
	
	@Test
	public void testDesactivation() {
		AdapteurLumiere a1 = new AdapteurLumiere();
		Telecommande t = new Telecommande();
		t.ajouterMulti(a1);
		t.desactiverMulti(a1);
		assertEquals("mauvaise desactivation", false , a1.isAllume());
	}
	
	@Test
	public void testDesactivationv1() {
		AdapteurLumiere a1 = new AdapteurLumiere();
		Telecommande t = new Telecommande();
		t.ajouterMulti(a1);
		t.desactiverMulti(a1);
		t.desactiverMulti(a1);
		assertEquals("mauvaise desactivation", false , a1.isAllume());
	}
	
	@Test
	public void testActiveTout() {
		AdapteurLumiere a1 = new AdapteurLumiere();
		AdapteurLumiere a2 = new AdapteurLumiere();
		AdapteurLumiere a3 = new AdapteurLumiere();
		Telecommande t = new Telecommande();
		t.ajouterMulti(a1);
		t.ajouterMulti(a2);
		t.ajouterMulti(a3);
		t.activerTout();
		assertEquals("mauvaise activation", true, a1.isAllume());
		assertEquals("mauvaise activation", true, a2.isAllume());
		assertEquals("mauvaise activation", true, a3.isAllume());
	}
	
	@Test
	public void testString() {
		AdapteurLumiere a1 = new AdapteurLumiere();
		AdapteurLumiere a2 = new AdapteurLumiere();
		Telecommande t = new Telecommande();
		t.ajouterMulti(a1);
		t.ajouterMulti(a2);
		String res2 = "lumiere: 0\nlumiere: 0\n";
		String res = "lumiere: 0";
		assertEquals("caca", res, a1.toString());
		assertEquals("caca", res2, t.toString());
	}

}
